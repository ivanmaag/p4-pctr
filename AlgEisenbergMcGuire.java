
/**
 * Práctica 4 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.concurrent.*;

public class AlgEisenbergMcGuire implements Runnable {

    public static volatile int[] flags = new int[2]; // 0 está ocioso, 1 esperando para entrar, 2 en sección crítica
    public static volatile int Turno = 0;
    public volatile int i;
    public volatile int j;
    public static volatile int nVueltas = 200;
    public static volatile int n = 0;

    /**
     * Constructor parametrizado.
     * @param i 
     */
    public AlgEisenbergMcGuire(int i) {
        this.i = i;
    }

    /**
     * Sobreescritura del metodo run.
     */
    public void run() {
        for (int p = 0; p < nVueltas; p++) {
            do {
                flags[i] = 1;
                j = Turno;
                while (j != i) {
                    if (flags[j] != 0)
                        j = Turno;
                    else
                        j = (j + 1) % 2;
                }

                flags[i] = 2;

                j = 0;
                while ((j < 2) && ((j == i) || (flags[j] != 2))) {
                    j = j + 1;
                }
            } while (!((j >= 2) && ((Turno == i) || (flags[Turno] == 0))));

            Turno = i;

            ++n;

            j = (Turno + 1) % 2;
            while (flags[j] == 0) {
                j = (j + 1) % 2;
            }

            Turno = j;
            flags[i] = 0;
        }
    }

    /**
     * Método que es lanzado al ejecutar el programa. Lanza 3 hilos que incrementan y decrementan una variable común.
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService ejecutor = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 2; i++)
            ejecutor.execute(new AlgEisenbergMcGuire(i));
        ejecutor.shutdown();

        System.out.println("Resultado de la variable común: " + n);
        System.out.println("El resultado debería ser: 400");
    }
}
