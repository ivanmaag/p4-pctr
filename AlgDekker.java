
/**
 * Práctica 4 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

class AlgDekker extends Thread {

    /* Iteraciones que dara cada hilo */
    static final int iteraciones = 2000000;
    /* Recurso compartido */
    static volatile int enteroCompartido = 0;
    /* Representa el deseo del hilo P1 de entrar en la seccion critica */
    static volatile boolean p1qe = false;
    /* Representa el deseo del hilo P2 de entrar en la seccion critica */  
    static volatile boolean p2qe = false;
    /* Representa el deseo del hilo P3 de entrar en la seccion critica */  
    static volatile boolean p3qe = false;
    /* Vector de hilos retenidos */
    private static volatile int[] retenidos = new int[3]; //

    /* Representa de quien es el turno */
    static volatile int turno = 1;

    class P1 extends Thread {
        public void run() {
            for (int i=0; i<iteraciones; ++i) {
                /* Seccion no critica */
                p1qe = true;
                while (p2qe || p3qe) {
                    if (turno != 1) {
                        p1qe = false;
                        while (turno != 1)
                            Thread.yield();
                        p1qe = true;
                    }
                }
                
                /* Seccion critica */
                ++enteroCompartido;
                /* Fin Seccion critica */
                
                if (retenidos.length > 0)
                    turno = extrae(retenidos);
                else
                    turno = 2;
                    
                p1qe = false;
            }
        }
    }
    
    class P2 extends Thread {
        public void run() {
            for (int i=0; i<iteraciones; ++i) {
                /* Seccion no critica */
                p2qe = true;
                while (p1qe || p3qe) {
                    if (turno != 2) {
                        p2qe = false;
                        while (turno != 2)
                            Thread.yield();
                        p2qe = true;
                    }
                }
                
                /* Seccion critica */
                --enteroCompartido;
                /* Fin Seccion critica */
                
                if (retenidos.length > 0)
                    turno = extrae(retenidos);
                else
                    turno = 3;
                    
                p2qe = false;
            }
        }
    }

    class P3 extends Thread {
        public void run() {
            for (int i=0; i<iteraciones; ++i) {
                /* Seccion no critica */
                p3qe = true;
                while (p1qe || p2qe) {
                    if (turno != 3) {
                        p3qe = false;
                        while (turno != 3)
                            Thread.yield();
                        p3qe = true;
                    }
                }
                
                /* Seccion critica */
                ++enteroCompartido;
                --enteroCompartido;
                /* Fin Seccion critica */
                
                if (retenidos.length > 0)
                    turno = extrae(retenidos);
                else
                    turno = 3;
                    
                p3qe = false;
            }
        }
    }


    AlgDekker() {
        Thread p1 = new P1();
        Thread p2 = new P2();
        Thread p3 = new P3();
        p1.start();
        p2.start();
        p3.start();
        
        try {
            p1.join();
            p2.join();
            p3.join();
            System.out.println("El valor del recurso compartido es " + enteroCompartido);
            System.out.println("Deberia ser 0.");
        }
        catch (InterruptedException e) {}
    }

    public static void main(String[] args) {
        new AlgDekker();
    }
}
