
/**
 * Práctica 4 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AlgLamport implements Runnable {

    public static volatile int nHilos;
    public static volatile int[] numero;
    public static volatile boolean[] eligiendo;
    public volatile int i;
    public volatile int mayor = 0;
    public static volatile int nVueltas = 20;
    public static volatile int n = 0;

    //Constructor nulo
    public AlgLamport() {}

    /**
     * Constructor parametrizado.
     * @param i
     * @param nHilos número de hilos.
     */
    public AlgLamport(int i,
                      int nHilos) {

        this.i = i;
        this.nHilos = nHilos;
    }

    /**
     * Sobreescritura del método run.
     */
    public void run() {
        for (int p = 0; p < nVueltas; p++) {
            eligiendo[i] = true;
            for (int j = 0; j < i; j++) {
                if (mayor < numero[j])
                    mayor = numero[j];
            }
            numero[i] = 1 + mayor;
            eligiendo[i] = false;

            for (int j = 0; j < i; j++) {
                while (eligiendo[j]) {}
                while ((numero[j] != 0) && (numero[j] < numero[i])) {}
            }

            /* Sección crítica */
            n++;
            /* Fin de sección crítica */

            numero[i] = 0;
        }
    }

    /**
     * Método que es lanzado al ejecutar el programa. Lanza 3 hilos que incrementan y decrementan una variable común.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int nHilos;
        System.out.print("Introduzca el numero de hilos deseados: ");
        nHilos = sc.nextInt();
        numero = new int[nHilos];
        eligiendo = new boolean[nHilos];
        for (int p = 0; p < nHilos; p++) {
            numero[p] = 0;
            eligiendo[p] = false;
        }

        ExecutorService ejecutor = Executors.newFixedThreadPool(nHilos);
        for (int i = 0; i < nHilos; i++)
            ejecutor.execute(new AlgLamport(i, nHilos));
        ejecutor.shutdown();

        System.out.println("Resultado de la variable común: " + n);
        sc.close();
    }
}
