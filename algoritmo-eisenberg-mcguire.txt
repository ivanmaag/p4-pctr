Repetir {

    /* Anunciar que necesitamos el recurso */
    flags[i] := ESPERANDO;

    /* Escanear los procesos partiendo desde el que posee el turno */
    /* Repite hasta encontrar todos los procesos en IDLE */
    indice := turno;
    mientras (indice != i) {
        Si (flags[índice] != IDLE)
            indice := turno;
        Si_no
            índice := (indice+1) mod n;
    }

    /* Reclamamos temporalmente el recurso */
    flags[i] := ACTIVO;

    /* Encontrar el primer proceso activo además de nosotros, si existe */
    indice := 0;
    mientras ((índice < n) && ((indice = i) || (flags[indice] != ACTIVO))) {
    indice := indice+1;
    }

/* Si no hay otros procesos activos, y tenemos el turno, o si todos los demás tienen
estado IDLE, proceder, en otro caso, repetir */
} Hasta que ((indice >= n) && ((turno = i) || (flags[turno] = IDLE)));

/* INICIO SECCIÓN CRÍTICA */

/* Reclamar el turno y proceder */
turno := i;

/* Código de Sección Crítica */

/* FIN de SECCIÓN CRÍTICA */

/* Encuentra un proceso que no esté IDLE */
/* (Si no hay otro nos encontraremos a nosotros mismos.) */
indice := (turno+1) mod n;
mientras (flags[indice] = IDLE) {
    indice := (indice+1) mod n;
}

/* Dar el turno a una hebra que lo necesita, o mantenerlo */
turno := indice;

/* Hemos acabado */
flags[i] := IDLE;

/* Código restante */
