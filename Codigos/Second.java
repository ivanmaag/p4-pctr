/* Adaptado de M. Ben-Ari por Manuel Francisco */

/* Segundo intento */
class Second {
	/* Iteraciones que dara cada hilo */
    static final int iteraciones = 2000000;
    /* Recurso compartido */
    static volatile int enteroCompartido = 0;
    /* Representa el deseo del hilo P de entrar en la seccion critica */
    static volatile boolean wantp = false;
    /* Representa el deseo del hilo Q de entrar en la seccion critica */ 
    static volatile boolean wantq = false;

    class P extends Thread {
        public void run() {
            for (int i=0; i<iteraciones; ++i) {
                /* Seccion no critica */
                while (wantq)
                    Thread.yield();
                wantp = true;
                
                /* Seccion critica */
                ++enteroCompartido;
                /* Fin Seccion critica */

                wantp = false;
            }
        }
    }
    
    class Q extends Thread {
        public void run() {
            for (int i=0; i<iteraciones; ++i) {
                /* Seccion no critica */
                while (wantp)
                    Thread.yield();
                wantq = true;
                
                /* Seccion critica */
                --enteroCompartido;
                /* Fin Seccion critica */
                
                wantq = false;
            }
        }
    }

    Second() {
        Thread p = new P();
        Thread q = new Q();
        p.start();
        q.start();
        
        try {
            p.join();
            q.join();
            System.out.println("El valor del recurso compartido es " + enteroCompartido);
            System.out.println("Deberia ser 0.");
        }
        catch (InterruptedException e) {}
    }

    public static void main(String[] args) {
        new Second();
    }
}
