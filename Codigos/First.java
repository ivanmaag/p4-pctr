/* Adaptado de M. Ben-Ari por Manuel Francisco */

/* Primer intento */
class First {
    /* Iteraciones que dara cada hilo */
    static final int iteraciones = 2000000;
    /* Recurso compartido */
    public static volatile int enteroCompartido = 0;
    /* Representa de quien es el turno */
    static volatile int turn = 1;

    class P extends Thread {
        public void run() {
            for (int i=0; i<iteraciones; ++i) {
                /* Seccion no critica */
                while (turn != 1)
                    Thread.yield();
                
                /* Seccion critica */
                ++enteroCompartido;
                /* Fin Seccion critica */
                
                turn = 2;
            }
        }
    }
    
    class Q extends Thread {
        public void run() {
            for (int i=0; i<iteraciones; ++i) {
                /* Seccion no critica */
                while (turn != 2)
                    Thread.yield();
                
                /* Seccion critica */
                --enteroCompartido;
                /* Fin Seccion critica */
                
                turn = 1;
            }
        }
    }

    First() {
        Thread p = new P();
        Thread q = new Q();
        p.start();
        q.start();
        
        try {
            p.join();
            q.join();
            System.out.println("El valor del recurso compartido es " + enteroCompartido);
            System.out.println("Deberia ser 0.");
        }
        catch (InterruptedException e) {}
    }

    public static void main(String[] args) {
        new First();
    }
}
